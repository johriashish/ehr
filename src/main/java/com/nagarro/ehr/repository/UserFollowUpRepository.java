package com.nagarro.ehr.repository;

import java.util.Set;

import org.springframework.data.repository.Repository;

import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.entity.UserFollowUp;

@org.springframework.stereotype.Repository
public interface UserFollowUpRepository extends Repository<UserFollowUp, Long>{

	UserFollowUp findOne(Long id);
	UserFollowUp save(UserFollowUp usertProfile);
	Set<UserFollowUp> findByUser(User user);
}
