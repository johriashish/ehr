package com.nagarro.ehr.repository;

import org.springframework.data.repository.Repository;

import com.nagarro.ehr.entity.Report;

@org.springframework.stereotype.Repository
public interface ReportRepository extends Repository<Report, Long>{

	Report findOne(Long id);
	Report save(Report user);
	
}
