package com.nagarro.ehr.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.nagarro.ehr.entity.User;

@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<User, Long>{

	User findOne(Long id);
	User findByEmailId(String emailId);
	List<User> findByFirstNameInIgnoreCase(String firstName);
	List<User> findByLastNameInIgnoreCase(String lastName);
	User save(User user);
	List<User> findAll(); 
	
}
