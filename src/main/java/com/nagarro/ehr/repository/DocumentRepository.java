package com.nagarro.ehr.repository;

import org.springframework.data.repository.Repository;

import com.nagarro.ehr.entity.Document;

@org.springframework.stereotype.Repository
public interface DocumentRepository extends Repository<Document, Long>{

	Document findOne(Long id);
	Document save(Document doc);
}
