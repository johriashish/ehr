package com.nagarro.ehr.repository;

import org.springframework.data.repository.Repository;

import com.nagarro.ehr.entity.Role;

import java.util.List;

@org.springframework.stereotype.Repository
public interface RoleRepository extends Repository<Role, Long>{

	Role save(Role role);
	Role findOne(Long id);
	Role findByCode(String code);
	List<Role> findAll();
}
