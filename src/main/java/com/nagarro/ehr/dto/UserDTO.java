package com.nagarro.ehr.dto;

import java.util.Date;

public class UserDTO {
	
	private String emailId;
	private String phone;
	private Long roleId;
	private String firstName;
	private String lastName;
	private String address;
	private Date dob;
	private Date registrationDate;

    public UserDTO() {
    }

    public UserDTO(String emailId,  String phone,
                   Long roleId, String firstName, String lastName,
                   String address, Date dob, Date registrationDate) {
        this.emailId = emailId;
        this.phone = phone;
        this.roleId = roleId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.dob = dob;
        this.registrationDate = registrationDate;
    }

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
}
