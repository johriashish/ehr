package com.nagarro.ehr.dto;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

public class ReportDTO {
	
	private int reportId;
	private int userId;
	private String title;
	private String allergy;
	private Date createDate;
	private int doctorId;
	private String diagnoseLab;
	public int getReportId() {
		return reportId;
	}
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
	public String getDiagnoseLab() {
		return diagnoseLab;
	}
	public void setDiagnoseLab(String diagnoseLab) {
		this.diagnoseLab = diagnoseLab;
	}
	
	
	
}
