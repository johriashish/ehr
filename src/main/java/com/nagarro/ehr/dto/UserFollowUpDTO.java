package com.nagarro.ehr.dto;


public class UserFollowUpDTO {

	private String summary;
	private String description;
	private Long userID;
	
	
	public UserFollowUpDTO() {
		super();
	}
	public UserFollowUpDTO(String summary, String description, Long userID) {
		super();
		this.summary = summary;
		this.description = description;
		this.userID = userID;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}

	
}
