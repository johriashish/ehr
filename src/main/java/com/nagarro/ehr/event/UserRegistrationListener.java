package com.nagarro.ehr.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.nagarro.ehr.service.NotificationService;

@Component
public class UserRegistrationListener  {
    private static final Logger log = LoggerFactory.getLogger(UserRegistrationListener.class);

	@Autowired
	NotificationService notificationService;

    @EventListener
    @Async
    public void onApplicationEvent(UserRegisterationEvent event) {
    	notificationService.sendEmail(event.getUser());
    	notificationService.sendSMS(event.getUser());
        log.info("New User Create Event..", event.getUser());
    }
}