 package com.nagarro.ehr.event;
import org.springframework.context.ApplicationEvent;

import com.nagarro.ehr.entity.User;
 
public class UserRegisterationEvent extends ApplicationEvent {
    public UserRegisterationEvent(User user) {
        super(user);
    }
 
    public User getUser() {
        return (User) getSource();
    }
 
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
 
    private static final long serialVersionUID = -5594735946614874286L;
 
}