package com.nagarro.ehr.service.impl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nagarro.ehr.dummys3.client.DummyAmazonclient;
import com.nagarro.ehr.entity.Document;
import com.nagarro.ehr.exception.FileArchiveServiceException;

@Service
public class DocumentUploadService {
	private static final Logger log = LoggerFactory.getLogger(DocumentUploadService.class);

	@Autowired
	private DummyAmazonclient s3Client;
	private static final String S3_BUCKET_NAME = "ehrS3";

	/**
	 * Save image to S3 and return CustomerImage containing key and public URL
	 * 
	 * @param multipartFile
	 * @return
	 * @throws IOException
	 */
	public String saveFileToS3(String key , MultipartFile multipartFile) throws FileArchiveServiceException {
		log.info("Saving file to S3");
		try{
			File fileToUpload = convertFromMultiPart(multipartFile);
			String path = s3Client.putObject(S3_BUCKET_NAME, key, fileToUpload);
			return path;
		}
		catch(Exception ex){
			log.error("Error occured while saving in S3", ex);
			throw new FileArchiveServiceException();
		}		
	}

	/**
	 * Delete image from S3 using specified key
	 * 
	 * @param customerImage
	 */
	public void deleteImageFromS3(Document document){
		s3Client.deleteObject(document.getKey());	
	}

	/**
	 * Convert MultiPartFile to ordinary File
	 * 
	 * @param multipartFile
	 * @return
	 * @throws IOException
	 */
	private File convertFromMultiPart(MultipartFile multipartFile) throws IOException {

		File file = new File(multipartFile.getOriginalFilename());
		file.createNewFile(); 
		FileOutputStream fos = new FileOutputStream(file); 
		fos.write(multipartFile.getBytes());
		fos.close(); 

		return file;
	}
}