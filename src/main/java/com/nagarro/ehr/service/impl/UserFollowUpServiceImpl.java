package com.nagarro.ehr.service.impl;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nagarro.ehr.entity.Document;
import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.entity.UserFollowUp;
import com.nagarro.ehr.exception.EntityNotPresent;
import com.nagarro.ehr.repository.UserFollowUpRepository;
import com.nagarro.ehr.service.DocumentService;
import com.nagarro.ehr.service.UserFollowUpService;
import com.nagarro.ehr.service.UserService;

@Service
public class UserFollowUpServiceImpl implements UserFollowUpService {

    private static final Logger log = LoggerFactory.getLogger(UserFollowUpServiceImpl.class);
    @Autowired
    private UserFollowUpRepository userFollowUpRepository;
    @Autowired
    private DocumentService documentService;

    @Autowired
    private UserService usertService;

    @Override
    public UserFollowUp saveUserFollowUp(UserFollowUp followUp) {
        return userFollowUpRepository.save(followUp);
    }

    @Override
    public UserFollowUp saveUserDocument(Long userFollowUpId, MultipartFile file) {
        log.info("Saving user document..");
        Document document = documentService.saveDocumentFile(file);
        UserFollowUp followUp = userFollowUpRepository.findOne(userFollowUpId);
        document.setUserFollowUp(followUp);
        documentService.saveDocument(document);
        followUp.getPatientDocs().add(document);
        return userFollowUpRepository.save(followUp);
    }

    @Override
    public Set<UserFollowUp> getUserFollowUps(Long userId) throws EntityNotPresent {
        User user = usertService.findOne(userId);
        return userFollowUpRepository.findByUser(user);
    }

    @Override
    public UserFollowUp getFollowUp(Long followUpId) {
        return userFollowUpRepository.findOne(followUpId);
    }
}