package com.nagarro.ehr.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.repository.UserRepository;
import com.nagarro.ehr.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	
    private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
    
    @Autowired
    private UserRepository repository;

    @Override
    public User login(User user) {
        logger.info("User Login called for:", user);
        User authenticatedUser = repository.findByEmailId(user.getEmailId());
        if (authenticatedUser.getPassword().equals(user.getPassword())) {
            logger.info("User is authenticated");
            return authenticatedUser;
        }
        logger.debug("Not a valid user");
        return null;
    }
}