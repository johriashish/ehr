package com.nagarro.ehr.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.service.NotificationService;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class NotificationServiceImpl implements NotificationService {
    private static final Logger log = LoggerFactory.getLogger(NotificationServiceImpl.class);
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(User user) {
        MimeMessage mail = javaMailSender.createMimeMessage();
        log.info("Sending Mail..");
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmailId());
            helper.setReplyTo("johri.ashish87@yahoo.com");
            helper.setFrom("johri.ashish87@yahoo.com");
            helper.setSubject("User created for EHR");
            String msg = "Hi " + user.getFirstName() + " " + user.getLastName() + "\n\n\n" +
                    " User for EHR apllication is created successfully.\n" +
                    " Please use following details to log in to the system.\n" +
                    " Url: https://abc.com/xyz \n" +
                    " Username: " + user.getEmailId() + "\n" +
                    " Password: " + user.getPassword() + "\n" +
                    "\n\n" +
                    " Thanks For Visiting:\n" +
                    " Admin EHR ";
            helper.setText(msg);
        } catch (MessagingException e) {
            log.error("Error occured while sending mail", e);
        } finally {
        }
        javaMailSender.send(mail);
    }

    @Override
    public void sendSMS(User user) {
        log.info("Sending SMS..", user);
    }

}
