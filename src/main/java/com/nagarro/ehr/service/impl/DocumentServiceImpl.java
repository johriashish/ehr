package com.nagarro.ehr.service.impl;

import java.io.File;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nagarro.ehr.entity.Document;
import com.nagarro.ehr.entity.UserFollowUp;
import com.nagarro.ehr.exception.FileArchiveServiceException;
import com.nagarro.ehr.repository.DocumentRepository;
import com.nagarro.ehr.service.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService {
	private static final Logger logger = LoggerFactory.getLogger(DocumentServiceImpl.class);

	@Autowired
    private DocumentRepository documentRepository;

	@Autowired
	DocumentUploadService uploadService;
	
	@Override
	public Document saveDocumentFile(MultipartFile multipartFile) {
		UserFollowUp followUp = new UserFollowUp();
		Document document = new Document();
		document.setUserFollowUp(followUp);
		File file = new File("");
		logger.info("Saving document..");
		String key = Instant.now().getEpochSecond() + "_" + file.getName();
		document.setKey(key);
		String path;
		try {
			path = uploadService.saveFileToS3(key  , multipartFile);
			document.setValue(path);
			logger.info("Document saved");
			return  document;
		} catch (FileArchiveServiceException e) {
			logger.info("Error occured while saving document",e);
		}
		return null;
	}

	@Override
	public Document saveDocument(Document document) {
		return documentRepository.save(document);
	}





    


}