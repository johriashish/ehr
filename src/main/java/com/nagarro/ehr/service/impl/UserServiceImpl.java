package com.nagarro.ehr.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.dto.UserDTO;
import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.event.UserRegisterationEvent;
import com.nagarro.ehr.exception.EntityAlreadyExists;
import com.nagarro.ehr.exception.EntityAlreadyExists.EntityType;
import com.nagarro.ehr.exception.EntityNotPresent;
import com.nagarro.ehr.repository.RoleRepository;
import com.nagarro.ehr.repository.UserRepository;
import com.nagarro.ehr.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private RoleRepository roleRepo;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    @Transactional
    public User save(final UserDTO userDto) throws EntityAlreadyExists {
        if (null == userRepo.findByEmailId(userDto.getEmailId())) {
            User user = new User();
            user.setEmailId(userDto.getEmailId());
            user.setPassword(this.getRandomPassword());
            user.setPhone(userDto.getPhone());
            user.setRole(roleRepo.findOne(userDto.getRoleId()));
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setAddress(userDto.getAddress());
            user.setDob(userDto.getDob());
            user.setRegistrationDate(new Date());
            user = userRepo.save(user);
            log.info("New user Created, Publishing custom event. ");
            UserRegisterationEvent userRegisterationEvent = new UserRegisterationEvent(user);
            eventPublisher.publishEvent(userRegisterationEvent);
            return user;
        } else {
            log.debug("User with this Email ID already exists");
            throw new EntityAlreadyExists(EntityAlreadyExists.EntityType.USER,
                    "User with this Email ID already exists");
        }
    }

    public String getRandomPassword() {
        Random r = new Random();
        int Low = 10000;
        int High = 99999;
        int result = r.nextInt(High - Low) + Low;
        return result + "";
    }

    @Override
    @Transactional
    public User findOne(final Long id) throws EntityNotPresent {
        User user = userRepo.findOne(id);
        if (null != user) {
            return user;
        }
        log.debug("User doesnt exist with user id : " + id);
        throw new EntityNotPresent(EntityType.USER,
                "User doesnt exist with user id : " + id);
    }

    @Override
    @Transactional
    public User findByEmailId(String emailId) throws EntityNotPresent {
        User user = userRepo.findByEmailId(emailId);
        if (null != user) {
            return user;
        }
        log.debug("User doesnt exist with emaild id  : " + emailId);
        throw new EntityNotPresent(EntityType.USER,
                "User doesnt exist with emaild id  : " + emailId);
    }

    @Override
    @Transactional
    public List<User> findAll() {
        return userRepo.findAll();
    }

    @Override
    public List<User> findByName(String name) throws EntityNotPresent {
        List<User> usersByFirstName = userRepo.findByFirstNameInIgnoreCase(name);
        List<User> usersByLastName = userRepo.findByLastNameInIgnoreCase(name);
        if (usersByFirstName.size() > 0 || usersByLastName.size() > 0) {
            usersByFirstName.addAll(usersByLastName);
            return usersByFirstName;
        }
        log.debug("User doesnt exist with name  : " + name);
        throw new EntityNotPresent(EntityType.USER,
                "User doesnt exist with name  : " + name);
    }

    @Override
    public User saveOrUpdate(User user) {
        return userRepo.save(user);
    }
}