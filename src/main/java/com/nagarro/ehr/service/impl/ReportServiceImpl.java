package com.nagarro.ehr.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.Report;
import com.nagarro.ehr.repository.ReportRepository;
import com.nagarro.ehr.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService {
    private static final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

	@Autowired
    private ReportRepository repository;

    @Override
    @Transactional
    public Report save(final Report report) {
    	return repository.save(report);
    }
    
    @Override
    @Transactional
    public Report findOne(final Long id) {
    	return repository.findOne(id);
    }
    


}