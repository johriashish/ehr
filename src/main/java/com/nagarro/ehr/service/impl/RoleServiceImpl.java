package com.nagarro.ehr.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.Role;
import com.nagarro.ehr.exception.EntityAlreadyExists;
import com.nagarro.ehr.exception.EntityAlreadyExists.EntityType;
import com.nagarro.ehr.repository.RoleRepository;
import com.nagarro.ehr.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {
    private static final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleRepo;

    @Override
    public Role save(Role role) throws EntityAlreadyExists {
        if (null == roleRepo.findByCode(role.getCode())) {
            return roleRepo.save(role);
        } else {
            log.error("Role with code " + role.getCode() + "already exists");
            throw new EntityAlreadyExists(EntityType.ROLE, "Role with code " + role.getCode() + "already exists");
        }
    }

    @Override
    public List<Role> getAll() {
        return roleRepo.findAll();
    }

    @Override
    public Role getByCode(String code) throws EntityAlreadyExists {
        Role role = roleRepo.findByCode(code);
        if (null != role)
            return role;
        else{
            log.error("Role with code " + code + "doesn't exists");
            throw new EntityAlreadyExists(EntityType.ROLE, "Role with code " + code + "doesn't exists");
        }
    }
}