package com.nagarro.ehr.service.impl;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.ehr.dto.UserDTO;
import com.nagarro.ehr.entity.Role;
import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.exception.EntityAlreadyExists;
import com.nagarro.ehr.service.RoleService;
import com.nagarro.ehr.service.SetupDB;
import com.nagarro.ehr.service.UserService;

/**
 * Created by Ashish Johri on 22-10-2016.
 */
@Service
public class SetupDBImpl implements SetupDB {
	private static final Logger log = LoggerFactory.getLogger(SetupDBImpl.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

/*	@Autowired
	private UserFollowUpService followUpService;*/

	@PostConstruct
	@Override
	public void setupDB() {
		// BUILDING INITIAL ROLES
		log.debug("Initial Database setup started..");
		log.debug("Creating Roles..");
		try {
			roleService.save(new Role("ADMIN", "Admin"));
		} catch (EntityAlreadyExists e2) {
			log.error("ADMIN Role already exists ", e2);
		}
		try {
			roleService.save(new Role("PATIENT", "Patient"));
		} catch (EntityAlreadyExists e2) {
			log.error("PATIENT Role already exists ", e2);
		}
		try {
			roleService.save(new Role("LABOPERATOR", "LabOperator"));
		} catch (EntityAlreadyExists e2) {
			log.error("LABOPERATOR Role already exists ", e2);
		}

		// BUILDING INITIAL USERS
		log.debug("Creating Users..");
		UserDTO userDto = null;
		try {
			userDto = new UserDTO("admin@ehr.com", "1234567890",roleService.getByCode("ADMIN").getRoleId(), "EHR","Admin", "Y-Company, USA", new Date(),new Date());
			User user = userService.save(userDto);
			user.setPassword("12345");
			userService.saveOrUpdate(user);
			
			userDto = new UserDTO("aa@aa.com", "1234567890",roleService.getByCode("PATIENT").getRoleId(), "EHR","Admin", "Y-Company, USA", new Date(),new Date());
			user = userService.save(userDto);
			user.setPassword("12345");
			userService.saveOrUpdate(user);

		} catch (EntityAlreadyExists e2) {
			log.error("User already exists ",userDto, e2);
		}
/*
		UserFollowUp followUp = new UserFollowUp();
		followUp.setDescription("First Time");
		followUp.setFollowUpSummary("Viral Fever");
		followUp.setFollowUpOn(new Date());
		followUp.setUser(user);
		UserFollowUp savedFollowUp = followUpService.saveUserFollowUp(followUp);
		Document document = new Document();
		document.setKey("fever");
		document.setUserFollowUp(savedFollowUp);
		document.setValue("");

		File file = new File("E://a.png");
		DiskFileItem fileItem = new DiskFileItem("file", "text/plain", false,
				file.getName(), (int) file.length(), file.getParentFile());
		// fileItem.getOutputStream();
		
		 * MultipartFile multipartFile = new CommonsMultipartFile(fileItem);
		 * followUpService.saveUserDocument(savedFollowUp.getId(), file);
		 */
	}
}
