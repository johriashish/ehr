package com.nagarro.ehr.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.nagarro.ehr.dto.UserDTO;
import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.exception.EntityAlreadyExists;
import com.nagarro.ehr.exception.EntityNotPresent;

@Service
public interface UserService {

	User save(UserDTO user) throws EntityAlreadyExists;
	
	User saveOrUpdate(User user);

	User findOne(Long id) throws EntityNotPresent;

	List<User> findAll();

	User findByEmailId(String emailId) throws EntityNotPresent;
	
	List<User> findByName(String emailId) throws EntityNotPresent;

	
}
