package com.nagarro.ehr.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.Role;
import com.nagarro.ehr.exception.EntityAlreadyExists;

@Service
public interface RoleService {

	Role save(Role role) throws EntityAlreadyExists;
	List<Role> getAll();
	Role getByCode(String role) throws EntityAlreadyExists;

}
