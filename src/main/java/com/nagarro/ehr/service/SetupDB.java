package com.nagarro.ehr.service;

import org.springframework.stereotype.Service;

/**
 * Created by Ashish Johri on 22-10-2016.
 */
@Service
public interface SetupDB {
    void setupDB();
}
