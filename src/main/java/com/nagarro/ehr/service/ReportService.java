package com.nagarro.ehr.service;

import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.Report;

@Service
public interface ReportService {

	Report save(Report report);

	Report findOne(Long id);


}
