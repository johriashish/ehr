package com.nagarro.ehr.service;

import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nagarro.ehr.entity.UserFollowUp;
import com.nagarro.ehr.exception.EntityNotPresent;

@Service
public interface UserFollowUpService {

	UserFollowUp saveUserFollowUp(UserFollowUp followUp);
	UserFollowUp saveUserDocument(Long userFollowUpId , MultipartFile file);
	Set<UserFollowUp> getUserFollowUps(Long userId) throws EntityNotPresent;
	UserFollowUp getFollowUp(Long followUpId);
	
	
}
