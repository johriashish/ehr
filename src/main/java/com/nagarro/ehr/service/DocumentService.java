package com.nagarro.ehr.service;

import org.springframework.web.multipart.MultipartFile;

import com.nagarro.ehr.entity.Document;

public interface DocumentService {

	Document saveDocumentFile(MultipartFile multipartFile);
	Document saveDocument(Document document);
	
}
