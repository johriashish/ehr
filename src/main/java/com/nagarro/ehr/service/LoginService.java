package com.nagarro.ehr.service;

import org.springframework.stereotype.Service;

import com.nagarro.ehr.entity.User;

@Service
public interface LoginService {

	User login(User user);

}
