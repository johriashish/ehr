package com.nagarro.ehr.dummys3.client;

import java.io.File;

import org.springframework.stereotype.Service;

@Service
public interface DummyAmazonclient {

	String putObject(String bucketName , String key, File file);
	void deleteObject(String url);
	
}
