package com.nagarro.ehr.dummys3.client;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;

@Service
public class DummyAmazonService implements DummyAmazonclient {

	final String rootPath ="documentRepository";
	@Override
	public String putObject(String bucketName, String key, File file) {
		try {
			BufferedImage originalImage = ImageIO.read(file);
			File dir = new File(rootPath + File.separator + bucketName);
			if(!dir.exists()){
				dir.mkdirs();
			}
			File newFile = new File(dir.getPath()+File.separator+key+".png");
			ImageIO.write(originalImage, "jpg",newFile);
			return newFile.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public void deleteObject(String url) {
		// TODO Auto-generated method stub

	}
	
	/*public static void main(String[] args) {
		DummyAmazonService ds = new DummyAmazonService();
		File f = new File("E://a.png");
		ds.putObject("EHR-S3", "doc1.png", f);
	}*/

}