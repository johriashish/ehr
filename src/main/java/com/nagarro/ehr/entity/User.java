package com.nagarro.ehr.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue
    @Column(name="user_id")
    private Long userId;
    @Column(nullable = true)
    private String emailId;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String phone;
    @ManyToOne(optional = false)
    @JoinColumn(name = "roleId")
    private Role role;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = true)
    private String lastName;
    @Column(nullable = true)
    private String address;
    @Column(nullable = true)
    private Date dob;
    @Column(nullable = false)
    private Date registrationDate;
    @OneToMany(fetch = FetchType.LAZY , mappedBy="user" , cascade=CascadeType.ALL)
    @JsonManagedReference
    private Set<UserFollowUp> userFollowUp;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

	public Set<UserFollowUp> getUserFollowUp() {
		return userFollowUp;
	}

	public void setUserFollowUp(Set<UserFollowUp> userFollowUp) {
		this.userFollowUp = userFollowUp;
	}

}
