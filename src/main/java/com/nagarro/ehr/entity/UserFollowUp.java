package com.nagarro.ehr.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="UserFollowUp")
public class UserFollowUp {
	
	@Id
	@Column(name="userFollowUpId")
	@GeneratedValue
	private Long id;
	@Column(nullable = false)
	private String followUpSummary;
	@Column(nullable = true)
	private String description;
	@Column(nullable = false)
	private Date followUpOn;
	@OneToMany(fetch = FetchType.LAZY , mappedBy="userFollowUp" , cascade=CascadeType.ALL)
	@JsonManagedReference
	private Set<Document> patientDocs;
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="user_id")
	private User user;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Set<Document> getPatientDocs() {
		return patientDocs;
	}
	public void setPatientDocs(Set<Document> patientDocs) {
		this.patientDocs = patientDocs;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getFollowUpSummary() {
		return followUpSummary;
	}
	public void setFollowUpSummary(String followUpSummary) {
		this.followUpSummary = followUpSummary;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getFollowUpOn() {
		return followUpOn;
	}
	public void setFollowUpOn(Date followUpOn) {
		this.followUpOn = followUpOn;
	}
	
	
	
	
}
