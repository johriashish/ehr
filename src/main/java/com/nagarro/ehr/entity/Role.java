package com.nagarro.ehr.entity;

import javax.persistence.*;

/**
 * Created by Ashish Johri on 22-10-2016.
 */
@Entity
@Table(name="role", uniqueConstraints = @UniqueConstraint(columnNames = {"code"}))
public class Role {
	
    @Id
    @GeneratedValue
    private Long roleId;
    @Column(nullable = false)
    private String code;
    @Column(nullable = true)
    private String name;
    
    

    public Role() {
		super();
	}

	public Role(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
