package com.nagarro.ehr.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Report")
public class Report {
	
	@Id
	private Long reportId;
	@Column(nullable = false)
	private int userId;
	@Column(nullable = false)
	private String title;
	@Column(nullable = true)
	private String allergy;
	@Column(nullable = false)
	private Date createDate;
	@Column(nullable = true)
	private int doctorId;
	@Column(nullable = true)
	private String diagnoseLab;
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAllergy() {
		return allergy;
	}
	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getDoctorId() {
		return doctorId;
	}
	public void setDoctorId(int doctorId) {
		this.doctorId = doctorId;
	}
	public String getDiagnoseLab() {
		return diagnoseLab;
	}
	public void setDiagnoseLab(String diagnoseLab) {
		this.diagnoseLab = diagnoseLab;
	}
	
	
	
}
