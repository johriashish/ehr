package com.nagarro.ehr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="document")
public class Document {
	
	@Id
	@GeneratedValue
	@Column(name="docId")
	private Long id;
	private String key;
	private String value;
	@ManyToOne
	@JoinColumn(name="userFollowUpId")
	@JsonBackReference
	private UserFollowUp userFollowUp;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public UserFollowUp getUserFollowUp() {
		return userFollowUp;
	}
	public void setUserFollowUp(UserFollowUp userFollowUp) {
		this.userFollowUp = userFollowUp;
	}

}
