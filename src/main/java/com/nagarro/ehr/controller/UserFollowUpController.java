package com.nagarro.ehr.controller;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nagarro.ehr.dto.UserFollowUpDTO;
import com.nagarro.ehr.entity.UserFollowUp;
import com.nagarro.ehr.exception.EntityNotPresent;
import com.nagarro.ehr.service.UserFollowUpService;
import com.nagarro.ehr.service.UserService;

@RestController
public class UserFollowUpController {

    private static final Logger log = LoggerFactory.getLogger(UserFollowUpController.class);
    @Autowired
    UserFollowUpService userFollowUpService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/save/userfollowup", method = RequestMethod.POST)
    public UserFollowUp savePatientData(@RequestBody UserFollowUpDTO followUpDto) throws EntityNotPresent {
        UserFollowUp followUp = new UserFollowUp();
        followUp.setDescription(followUpDto.getDescription());
        followUp.setFollowUpSummary(followUpDto.getSummary());
        followUp.setUser(userService.findOne(followUpDto.getUserID()));
        followUp.setFollowUpOn(new Date());
        return userFollowUpService.saveUserFollowUp(followUp);
    }

    @RequestMapping(value = "/save/userFollowUp/doc/{followUpId}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public UserFollowUp savePatientDoc(MultipartHttpServletRequest req, @PathVariable(value = "followUpId") int followUpId) {
        Map<String, MultipartFile> files = ((MultipartHttpServletRequest) req).getFileMap();
        MultipartFile file = files.get("file");
        return userFollowUpService.saveUserDocument((long) followUpId, file);
    }

    @RequestMapping(value = "/get/userfollowup/{userId}", method = RequestMethod.GET)
    public Set<UserFollowUp> savePatientData(@PathVariable(value = "userId") Long userId) throws EntityNotPresent {
        return userFollowUpService.getUserFollowUps(userId);
    }

}
