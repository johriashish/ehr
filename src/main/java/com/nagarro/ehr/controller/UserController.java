package com.nagarro.ehr.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.exception.EntityNotPresent;
import com.nagarro.ehr.service.UserService;

@RestController
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	/*@RequestMapping(value="/user",method = RequestMethod.GET , produces = "application/json")
    public String user(){
        return "{\"data\":\"assdfhish\"}";
    }*/
	
	@RequestMapping(value="/viewReport",method = RequestMethod.GET , produces = "application/json")
    public String viewReport(){
        return "{\"data\":\"ashish\"}";
    }
	
	@RequestMapping(value="/viewProfile",method = RequestMethod.GET , produces = "application/json")
    public String viewProfile(){
        return "{\"data\":\"ashish\"}";
    }
	
	@RequestMapping(value="/editReport",method = RequestMethod.GET , produces = "application/json")
    public String editReport(){
        return "{\"data\":\"ashish\"}";
    }
	
	@RequestMapping(value="/fetchuser/{userId}",method = RequestMethod.GET)
    public User fetchUser(@PathVariable("userId") Long userId ) throws EntityNotPresent{
		User user = userService.findOne(userId);
		return user;
		
    }
    @RequestMapping(value="/fetchuser/emailId/{emailId:.+}",method = RequestMethod.GET)
    public User fetchUserByEmail(@PathVariable("emailId") String emailId ) throws EntityNotPresent{
        User user = userService.findByEmailId(emailId);
        return user;
    }
    
    @RequestMapping(value="/fetchuser/name/{name:.+}",method = RequestMethod.GET)
    public List<User> fetchUserByName(@PathVariable("name") String name ) throws EntityNotPresent{
        List<User> users = userService.findByName(name);
        return users;
    }
	
}
