package com.nagarro.ehr.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.nagarro.ehr.dto.UserDTO;
import com.nagarro.ehr.entity.Role;
import com.nagarro.ehr.entity.User;
import com.nagarro.ehr.exception.EntityAlreadyExists;
import com.nagarro.ehr.service.RoleService;
import com.nagarro.ehr.service.SetupDB;
import com.nagarro.ehr.service.UserService;

@RestController
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SetupDB setupDB;

    private static final Logger log = LoggerFactory.getLogger(AdminController.class);

    @RequestMapping(value = "/createAccount", method = RequestMethod.POST)
    public User createAccount(@RequestBody UserDTO user) throws EntityAlreadyExists {
        return userService.save(user);
    }

    @RequestMapping(value = "/createRole", method = RequestMethod.POST)
    public Role createRole(@RequestBody Role role) throws EntityAlreadyExists {
        return roleService.save(role);
    }

    @RequestMapping(value = "/allUsers", method = RequestMethod.GET, produces = "application/json")
    public List<User> allusers() {
        return userService.findAll();
    }

    @RequestMapping(value = "/allRoles", method = RequestMethod.GET)
    public List<Role> getAllRole() {
        return roleService.getAll();
    }

/*    @PostConstruct
    @RequestMapping(value = "/setupdb", method = RequestMethod.GET)
    public String setupDB() {
        setupDB.setupDB();
        return "Initial DB set succesful";
    }*/

    @ExceptionHandler
    void handleIllegalArgumentException(EntityAlreadyExists e, HttpServletResponse response) throws IOException {
        log.error("Error:",e);
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }
}
