package com.nagarro.ehr.exception;

import com.nagarro.ehr.exception.EntityAlreadyExists.EntityType;


public class EntityNotPresent extends Exception{

	private EntityType entityType;
	private String message;
	
	
	public EntityNotPresent(EntityType entityType, String message) {
		this.entityType = entityType;
		this.message = message;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
