package com.nagarro.ehr.exception;

public class EntityAlreadyExists extends Exception{

	public static enum EntityType {USER , ROLE}
	
	private EntityType entityType;
	private String message;
	
	
	public EntityAlreadyExists(EntityType entityType, String message) {
		super();
		this.entityType = entityType;
		this.message = message;
	}
	public EntityType getEntityType() {
		return entityType;
	}
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
