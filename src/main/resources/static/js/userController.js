app.controller('usersController', function($rootScope , $scope , UserService , fileUpload ,$location) {
    $scope.headingTitle = "User List";
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
  };
    if(sessionStorage.user == null || sessionStorage.user == undefined || sessionStorage.user == "" || sessionStorage.user == "undefined")
		$rootScope.userAuthenticated = false;
	else{
    	$rootScope.userAuthenticated = true;
    	$scope.user = angular.fromJson(sessionStorage.user);
    }
   var loginHandler = function(data){
		if(!isNaN(data.userId)){
			$scope.user = data;
			$rootScope.user = data;
			$rootScope.userAuthenticated = true;
			sessionStorage.user = angular.toJson(data);
			//localStorage.setItem('user', JSON.stringify(data));
			//localStorage.myObject = JSON.stringify(data);
			if(data.role.code=="PATIENT"){
				$location.path('/patient/home');
			}
			else{
				$location.path('/admin/home');
			}
		}
		else{
			//$scope.message  = 'Your score card has been mailed to you.'
			//alert(angular.toJson(data));
			$rootScope.userAuthenticated = false;
			sessionStorage.user = undefined;
			$rootScope.user = null;
			$scope.message = "Invalid User";
		}
		//$location.path('/testPaper');
	};

    $scope.login = function(){
    	UserService.login($scope,loginHandler)
    };
    
    $scope.logout = function(){
    	$rootScope.userAuthenticated = false;
    	sessionStorage.user = undefined;
		$rootScope.user = null;
		$scope.message = "User Logged out";
		$location.path('/logout');
    }
    
	var successHandlerMethod = function(data){
			if(data==null || data == ""){
				$scope.user  = 'Invalid User';
			}
			else{
				//$scope.message  = 'Your score card has been mailed to you.'
				//alert(angular.toJson(data));
				$scope.user = data;
			}
			//$location.path('/testPaper');
		};
	  //UserService.fetchUsers(successHandlerMethod);
		 var errorHandler = function(data){
		    	alert(data.message);
		    };
		    var userFollowUpdHandler = function(data){
		    	alert("Details saved");
		    };
		    
		    var showFollowUps = function(data){
		    	$scope.userFollowUps = data;
		    };
	    $scope.uploadFile = function(id){
	        var file = $scope.myFile;
	        console.log('file is ' );
	        console.dir(file);
	        var uploadUrl = "/fileUpload";
	        fileUpload.uploadFileToUrl(file, uploadUrl);
	    };
	    
	    $scope.saveFollowUpDetails = function(form) {
	        console.log("posting add follow up data....");
	        var formData = form;

	        
	        UserService.saveFollowUpDetails($scope,form , userFollowUpdHandler , errorHandler)
	        //$http.post('form.php', JSON.stringify(data)).success(function(){/*success callback*/});
	    };
	    
	    $scope.openImage = function(url){
	    	 window.location.href="file:///"+url;
	    };
	    $scope.fetchUserFollowUps = function(id){
	    	UserService.fetchFollowUps($scope , id,showFollowUps)
	    	
	    };

});