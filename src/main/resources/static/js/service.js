app.service('UserService', ['$http', function ($http ) {

    this.fetchUsers = function(successHandlerMethod){
		$http.get("http://localhost:9999/user").success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
    this.login = function($scope ,  successHandlerMethod){
		$http.post("http://localhost:9999/login" , $scope.user).success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
    this.fetchUser = function($scope ,  emailId , successHandlerMethod){
		$http.get("http://localhost:9999/fetchuser/emailId/"+emailId).success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
    this.fetchUserByEmail = function($scope ,  emailId , successHandlerMethod){
		$http.get("http://localhost:9999/fetchuser/emailId/"+emailId).success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
    this.fetchUserByName = function($scope ,  name , successHandlerMethod){
		$http.get("http://localhost:9999/fetchuser/name/"+name).success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
    this.createAccount = function($scope ,  data , successHandlerMethod , errorHandler){
		$http.post("http://localhost:9999/createAccount",data).success(function(data){
			successHandlerMethod(data);
		}).error(
				function(data){
					errorHandler(data);
				}
		);
    }
    this.saveFollowUpDetails = function($scope ,  data , successHandlerMethod , errorHandler){
		$http.post("http://localhost:9999//save/userfollowup",data).success(function(data){
			successHandlerMethod(data);
		}).error(
				function(data){
					errorHandler(data);
				}
		);
    }
    this.fetchFollowUps = function($scope ,  id , successHandlerMethod){
		$http.get("http://localhost:9999/get/userfollowup/"+id).success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
}]);

app.service('RoleService', ['$http', function ($http ) {

    this.fetchAllRoles = function($scope ,successHandlerMethod){
		$http.get("http://localhost:9999/allRoles").success(function(data){
			successHandlerMethod(data);
		}).error(function(error){alert("erros");});
    }
    
}]);

app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(id,file, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        $http.post("http://localhost:9999/save/userFollowUp/doc/"+id, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);