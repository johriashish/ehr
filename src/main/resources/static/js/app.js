var app = angular.module('app', ['ngRoute','ngResource']);
app.config(function($routeProvider){
    $routeProvider
	    .when('/patient/home',{
	        templateUrl: '/views/user.html',
	        controller: 'usersController'
	    })
        .when('/admin/home',{
            templateUrl: '/views/admin.html',
            controller: 'adminController'
        })
        .when('/roles',{
            templateUrl: '/views/roles.html',
            controller: 'rolesController'
        })
        .when('/login',{
            templateUrl: '/views/login.html',
            controller: 'usersController'
        })
        .when('/logout',{
            templateUrl: '/views/home.html',
            
        })
        .otherwise(
            { redirectTo: '/'}
        );
});

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

